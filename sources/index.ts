import { config as dotenvConfig } from 'dotenv';

dotenvConfig();

import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import session, { SessionOptions } from 'express-session';
import { createServer } from 'http';

import { sequelize } from 'main/database';
import { authMiddleware, responseMiddleware } from 'main/router/middlewares';
import { socketServer } from 'main/socket';
import { routes } from 'main/router/routes';

const app = express();

const corsConfig = {
  credentials: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  origin: true,
  preflightContinue: false,
};

const sessionOptions: SessionOptions = {
  resave: false,
  name: 'session',
  saveUninitialized: false,
  secret: 'meow meow',
};

app.use(responseMiddleware.json());
app.use(cookieParser());
app.use(bodyParser.json({ limit: '16mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsConfig));
app.use(session(sessionOptions));
app.use(authMiddleware.initialize());
app.use(routes);
app.use(responseMiddleware.notFound());
app.use(responseMiddleware.error());
app.on('error', console.error);

const server = createServer(app);

server.on('error', (error) => {
  app.emit('Server error:', error);
});

const startServer = async () => {
  try {
    await sequelize.query('set foreign_key_checks=0');
    await sequelize.query(`set global sql_mode=(select replace(@@sql_mode,'ONLY_FULL_GROUP_BY',''))`);
    await sequelize.sync({ force: false });
    socketServer.attach(server);
    server.listen(process.env.PORT);
  } catch (error) {
    throw error;
  }
};

startServer()
  .then(() => console.log(`Listening on ${process.env.PORT}`))
  .catch(console.error);

process.on('unhandledRejection', (error) => {
  app.emit('error', error);
});
