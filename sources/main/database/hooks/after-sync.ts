import { ModelCtor } from 'sequelize-typescript';

export async function afterSync(this: ModelCtor, connection) {
  const queryInterface = connection.sequelize.getQueryInterface();
  const { rawAttributes, tableName } = this;
  const tableIndexes: { name: string, primary: boolean }[] = await queryInterface.showIndex(tableName);
  const tableIndexNames = tableIndexes.map(({ name }) => name);
  const customIndexNames = this['_indexes'].map(({ name }) => name);

  // Создание индексов для связей
  await Promise.all(
    Object
      .values(rawAttributes)
      .filter(({ field, references }) => !tableIndexNames.includes(field) && references != null)
      .map(({ field }) => queryInterface.addIndex(tableName, { name: field, fields: [field] })),
  );

  // Удаление автоматически созданных индексов для связей
  await Promise.all(
    tableIndexes
      .filter(({ name, primary }) => !customIndexNames.includes(name) && rawAttributes[name] == null && !primary)
      .map(({ name }) => queryInterface.removeIndex(tableName, name)),
  );
}
