import { Sequelize, SequelizeOptions } from 'sequelize-typescript';
import { SequelizeHooks } from 'sequelize/types/lib/hooks';

import * as hooks from 'main/database/hooks';
import * as models from 'main/database/models';

const sequelizeOptions: SequelizeOptions = {
  database: process.env.DB_NAME,
  define: {
    paranoid: true,
    timestamps: true,
  },
  dialect: 'mysql',
  logging: false,
  password: process.env.DB_PASSWORD,
  username: process.env.DB_USERNAME,
};

const sequelize = new Sequelize(sequelizeOptions);

sequelize.addModels(Object.values(models));

Object
  .keys(hooks)
  .forEach((hookType: keyof SequelizeHooks) => {
    sequelize.addHook(hookType, hooks[hookType]);
  });

export { models, sequelize };
