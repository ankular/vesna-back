import { AllowNull, BelongsToMany, Column, Comment, DataType, Default, HasMany, Model, Table } from 'sequelize-typescript';

import { User } from 'main/database/models/user';
import { ExampleAttachment } from 'main/database/models/example-attachment';
import { Example } from 'main/database/models/example';
import { Service } from 'main/database/models/service';

@Table
export class Attachment extends Model<Attachment> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Default('')
  @Comment('Путь до папки с файлом от папки attachments')
  @Column(DataType.STRING(256))
  public directory?: string = '';

  @AllowNull(false)
  @Column(DataType.STRING(128))
  public mime: string;

  @AllowNull(false)
  @Column(DataType.STRING(64))
  public name: string;

  @AllowNull(false)
  @Column
  public size: number;

  ////////////////////////////////////////////////////////////////
  // Виртуальные поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.VIRTUAL)
  public get isImage(): boolean {
    if (this.mime == null) {
      return undefined;
    }

    return this.mime.includes('image');
  }

  @Column(DataType.VIRTUAL)
  public get url(): string {
    if (this.id == null || this.name == null) {
      return undefined;
    }

    return `api/attachments/${this.id}_${this.name}`;
  }

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsToMany(() => Example, () => ExampleAttachment, 'attachmentId', 'exampleId')
  public examples?: Example[];

  @HasMany(() => Service, 'imageId')
  public serviceImages?: Service[];

  @HasMany(() => User, 'avatarId')
  public userAvatars?: User[];
}
