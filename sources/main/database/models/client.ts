import { AllowNull, BelongsTo, Column, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';

import { Order } from 'main/database/models/order';
import { User } from 'main/database/models/user';

@Table
export class Client extends Model<Client> {
  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => User)
  @Column
  public userId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @HasMany(() => Order, 'clientId')
  public orders?: Order[];

  @BelongsTo(() => User, 'userId')
  public user: User;
}
