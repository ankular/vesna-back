import { AllowNull, AutoIncrement, BelongsTo, Column, ForeignKey, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { Employee } from 'main/database/models/employee';
import { Position } from 'main/database/models/position';

@Table({
  indexes: [
    {
      fields: ['employeeId', 'positionId'],
      name: 'employeePositionUnique',
      unique: true,
    },
  ],
  timestamps: false,
})
export class EmployeePosition extends Model<EmployeePosition> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AutoIncrement
  @PrimaryKey
  @Column
  public id: number;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Employee)
  @Column
  public employeeId: number;

  @AllowNull(false)
  @ForeignKey(() => Position)
  @Column
  public positionId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Employee, 'employeeId')
  public employee: Employee;

  @BelongsTo(() => Position, 'positionId')
  public position: Position;
}
