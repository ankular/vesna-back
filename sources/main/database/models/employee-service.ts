import { AllowNull, AutoIncrement, BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { Employee } from 'main/database/models/employee';
import { Service } from 'main/database/models/service';
import { OrderEmployeeService } from 'main/database/models/order-employee-service';
import { Order } from 'main/database/models/order';

@Table({
  indexes: [
    {
      fields: ['employeeId', 'serviceId'],
      name: 'employeeServiceUnique',
      unique: true,
    },
  ],
  timestamps: false,
})
export class EmployeeService extends Model<EmployeeService> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AutoIncrement
  @PrimaryKey
  @Column
  public id: number;

  @Column(DataType.TIME)
  public leadTime?: string;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Employee)
  @Column
  public employeeId: number;

  @AllowNull(false)
  @ForeignKey(() => Service)
  @Column
  public serviceId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Employee, 'employeeId')
  public employee: Employee;

  @BelongsToMany(() => Order, () => OrderEmployeeService, 'employeeServiceId', 'orderId')
  public orders?: Order[];

  @HasMany(() => OrderEmployeeService, 'employeeServiceId')
  public orderEmployeeServices?: OrderEmployeeService[];

  @BelongsTo(() => Service, 'serviceId')
  public service: Service;
}
