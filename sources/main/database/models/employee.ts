import { AllowNull, BelongsTo, BelongsToMany, Column, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';

import { EmployeeService } from 'main/database/models/employee-service';
import { EmployeePosition } from 'main/database/models/employee-position';
import { Holiday } from 'main/database/models/holiday';
import { Position } from 'main/database/models/position';
import { Service } from 'main/database/models/service';
import { TimeSheet } from 'main/database/models/time-sheet';
import { User } from 'main/database/models/user';

@Table
export class Employee extends Model<Employee> {
  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => User)
  @Column
  public userId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @HasMany(() => EmployeeService, 'employeeId')
  public employeeServices?: EmployeeService[];

  @HasMany(() => Holiday, 'employeeId')
  public holidays?: Holiday[];

  @BelongsToMany(() => Position, () => EmployeePosition, 'employeeId', 'positionId')
  public positions?: Position[];

  @BelongsToMany(() => Service, () => EmployeeService, 'employeeId', 'serviceId')
  public services?: Service[];

  @HasMany(() => TimeSheet, 'employeeId')
  public timeSheets?: TimeSheet[];

  @BelongsTo(() => User, 'userId')
  public user: User;
}
