import { AllowNull, AutoIncrement, BelongsTo, Column, ForeignKey, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { Attachment } from 'main/database/models/attachment';
import { Example } from 'main/database/models/example';

@Table({
  indexes: [
    {
      fields: ['attachmentId', 'exampleId'],
      name: 'exampleAttachmentUnique',
      unique: true,
    },
  ],
  timestamps: false,
})
export class ExampleAttachment extends Model<ExampleAttachment> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AutoIncrement
  @PrimaryKey
  @Column
  public id: number;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Attachment)
  @Column
  public attachmentId: number;

  @AllowNull(false)
  @ForeignKey(() => Example)
  @Column
  public exampleId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Attachment, 'attachmentId')
  public attachment: Attachment;

  @BelongsTo(() => Example, 'exampleId')
  public example: Example;
}
