import { AllowNull, BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { Attachment } from 'main/database/models/attachment';
import { Employee } from 'main/database/models/employee';
import { ExampleAttachment } from 'main/database/models/example-attachment';
import { Service } from 'main/database/models/service';

@Table
export class Example extends Model<Example> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.TEXT)
  public description?: string;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Employee)
  @Column
  public employeeId: number;

  @AllowNull(false)
  @ForeignKey(() => Service)
  @Column
  public serviceId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsToMany(() => Attachment, () => ExampleAttachment, 'exampleId', 'attachmentId')
  public attachments?: Attachment[];

  @BelongsTo(() => Employee, 'employeeId')
  public employee: Employee;

  @BelongsTo(() => Service, 'serviceId')
  public service: Service;
}
