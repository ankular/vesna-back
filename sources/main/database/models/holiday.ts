import { AllowNull, BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { Employee } from 'main/database/models/employee';

@Table
export class Holiday extends Model<Holiday> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.TEXT)
  public description?: string;

  @AllowNull(false)
  @Column
  public endAt: Date;

  @AllowNull(false)
  @Column
  public startAt: Date;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Employee)
  @Column
  public employeeId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Employee, 'employeeId')
  public employee?: Employee;
}
