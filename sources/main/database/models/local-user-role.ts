import { AllowNull, AutoIncrement, BelongsTo, Column, ForeignKey, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { LocalUser } from 'main/database/models/local-user';
import { Role } from 'main/database/models/role';

@Table({
  indexes: [
    {
      fields: ['localUserId', 'roleId'],
      name: 'localUserRoleUnique',
      unique: true,
    },
  ],
  timestamps: false,
})
export class LocalUserRole extends Model<LocalUserRole> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AutoIncrement
  @PrimaryKey
  @Column
  public id: number;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => LocalUser)
  @Column
  public localUserId: number;

  @AllowNull(false)
  @ForeignKey(() => Role)
  @Column
  public roleId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => LocalUser, 'localUserId')
  public localUser: LocalUser;

  @BelongsTo(() => Role, 'roleId')
  public role: Role;
}
