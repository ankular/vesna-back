import { createHash } from 'crypto';
import { AllowNull, BeforeCreate, BeforeUpdate, BelongsTo, BelongsToMany, Column, DataType, DefaultScope, ForeignKey, HasMany, Model, Table, Unique } from 'sequelize-typescript';

import { LocalUserRole } from 'main/database/models/local-user-role';
import { Role } from 'main/database/models/role';
import { Session } from 'main/database/models/session';
import { User } from 'main/database/models/user';

const hashPassword = (password: string): string => {
  return createHash('sha1')
    .update(`${password}`)
    .digest('hex');
};

@DefaultScope(() => ({
  attributes: {
    exclude: ['password'],
  },
}))
@Table({
  timestamps: false,
})
export class LocalUser extends Model<LocalUser> {
  ////////////////////////////////////////////////////////////////
  // Статик-методы
  ////////////////////////////////////////////////////////////////

  @BeforeCreate
  @BeforeUpdate
  public static beforeUpsert(instance: LocalUser) {
    if (instance.password != null) {
      instance.password = hashPassword(instance.password);
    }
  }

  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @Column(DataType.STRING(64))
  public password: string;

  @AllowNull(false)
  @Unique
  @Column(DataType.STRING(64))
  public username: string;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => User)
  @Column
  public userId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => User, 'userId')
  public user: User;

  @BelongsToMany(() => Role, () => LocalUserRole, 'localUserId', 'roleId')
  public roles?: Role[];

  @HasMany(() => Session, 'localUserId')
  public sessions?: Session[];

  ////////////////////////////////////////////////////////////////
  // Методы
  ////////////////////////////////////////////////////////////////

  public checkPassword(password: string): boolean {
    return hashPassword(password) === this.password;
  }
}
