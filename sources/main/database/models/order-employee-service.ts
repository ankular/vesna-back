import { AllowNull, AutoIncrement, BelongsTo, Column, ForeignKey, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { Order } from 'main/database/models/order';
import { EmployeeService } from 'main/database/models/employee-service';
import { Client } from 'main/database/models/client';

@Table({
  indexes: [
    {
      fields: ['employeeServiceId', 'orderId'],
      name: 'orderEmployeeServiceUnique',
      unique: true,
    },
  ],
  timestamps: false,
})
export class OrderEmployeeService extends Model<OrderEmployeeService> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AutoIncrement
  @PrimaryKey
  @Column
  public id: number;

  @AllowNull(false)
  @ForeignKey(() => Client)
  @Column
  public executionAt: Date;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => EmployeeService)
  @Column
  public employeeServiceId: number;

  @AllowNull(false)
  @ForeignKey(() => Order)
  @Column
  public orderId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => EmployeeService, 'employeeServiceId')
  public employeeService: EmployeeService;

  @BelongsTo(() => Order, 'orderId')
  public order: Order;
}
