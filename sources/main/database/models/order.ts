import { AllowNull, BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';

import { Client } from 'main/database/models/client';
import { OrderEmployeeService } from 'main/database/models/order-employee-service';
import { EmployeeService } from 'main/database/models/employee-service';

@Table
export class Order extends Model<Order> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.FLOAT)
  public discount?: number

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Client)
  @Column
  public clientId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Client, 'clientId')
  public client: Client;

  @BelongsToMany(() => EmployeeService, () => OrderEmployeeService, 'orderId', 'employeeServiceId')
  public employeeServices?: EmployeeService[];

  @HasMany(() => OrderEmployeeService, 'orderId')
  public orderEmployeeServices?: OrderEmployeeService[];
}
