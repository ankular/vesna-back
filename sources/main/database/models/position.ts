import { BelongsToMany, Column, DataType, Model, Table } from 'sequelize-typescript';

import { Employee } from 'main/database/models/employee';
import { EmployeePosition } from 'main/database/models/employee-position';

@Table
export class Position extends Model<Position> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.TEXT)
  public description?: string;

  @Column(DataType.STRING(128))
  public name: string;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsToMany(() => Employee, () => EmployeePosition, 'positionId', 'employeeId')
  public employees?: Employee[];
}
