import { AllowNull, BelongsToMany, Column, DataType, Model, Table, Unique } from 'sequelize-typescript';

import { LocalUser } from 'main/database/models/local-user';
import { LocalUserRole } from 'main/database/models/local-user-role';

@Table
export class Role extends Model<Role> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @Unique
  @Column(DataType.STRING(64))
  public code: string;

  @AllowNull(false)
  @Column(DataType.STRING(128))
  public name: string;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsToMany(() => LocalUser, () => LocalUserRole, 'roleId', 'localUserId')
  public localUsers?: LocalUser[];
}
