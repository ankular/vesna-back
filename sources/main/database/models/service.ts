import {
  AllowNull,
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  Table,
} from 'sequelize-typescript';

import { Attachment } from 'main/database/models/attachment';
import { Employee } from 'main/database/models/employee';
import { EmployeeService } from 'main/database/models/employee-service';
import { ServiceType } from 'main/database/models/service-type';
import { Example } from 'main/database/models/example';

@Table
export class Service extends Model<Service> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.TEXT)
  public description?: string;

  @Column(DataType.STRING(128))
  public name: string;

  @Column(DataType.FLOAT)
  public price?: number;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @ForeignKey(() => Attachment)
  @Column
  public imageId?: number;

  @AllowNull(false)
  @ForeignKey(() => ServiceType)
  @Column
  public serviceTypeId: number;

  ////////////////////////////////////////////////////////////////
  // Виртуальные поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.VIRTUAL)
  public get imageUrl(): string {
    return this.image?.url ?? 'api/attachments/0_placeholder.png';
  }

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsToMany(() => Employee, () => EmployeeService, 'serviceId', 'employeeId')
  public employees?: Employee[];

  @HasMany(() => EmployeeService, 'serviceId')
  public employeeServices?: EmployeeService[];

  @BelongsTo(() => Attachment, 'imageId')
  public image?: Attachment;

  @BelongsTo(() => ServiceType, 'serviceTypeId')
  public serviceType: ServiceType;
}
