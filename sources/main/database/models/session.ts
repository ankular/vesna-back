import { BelongsTo, Column, DataType, ForeignKey, Model, Table, Unique } from 'sequelize-typescript';

import { LocalUser } from 'main/database/models/local-user';

@Table({
  timestamps: false,
})
export class Session extends Model<Session> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Unique
  @Column(DataType.STRING(128))
  public cookie: string;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @ForeignKey(() => LocalUser)
  public localUserId: string;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => LocalUser, 'localUserId')
  public localUser: LocalUser;
}
