import { AllowNull, BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';

import { Employee } from 'main/database/models/employee';

@Table
export class TimeSheet extends Model<TimeSheet> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @AllowNull
  @Column(DataType.DATEONLY)
  public endWorkDate?: string;

  @AllowNull(false)
  @Column(DataType.TIME)
  public endWorkTime: string;

  @AllowNull(false)
  @Column(DataType.DATEONLY)
  public startWorkDate: string;

  @AllowNull(false)
  @Column(DataType.TIME)
  public startWorkTime: string;

  @AllowNull(false)
  @Column(DataType.TINYINT)
  public weekendCount: number;

  @AllowNull(false)
  @Column(DataType.TINYINT)
  public workdayCount: number;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @AllowNull(false)
  @ForeignKey(() => Employee)
  @Column
  public employeeId: number;

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Employee, 'employeeId')
  public employee?: Employee;
}
