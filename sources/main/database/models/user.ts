import { DateTime } from 'luxon';
import { AllowNull, Column, DataType, Model, Table, BelongsTo, ForeignKey, HasOne } from 'sequelize-typescript';

import { Attachment } from 'main/database/models/attachment';
import { LocalUser } from 'main/database/models/local-user';

@Table
export class User extends Model<User> {
  ////////////////////////////////////////////////////////////////
  // Поля
  ////////////////////////////////////////////////////////////////

  @Column
  public bornAt?: Date;

  @Column
  @Column(DataType.STRING(64))
  public phone?: string;

  @AllowNull(false)
  @Column
  @Column(DataType.STRING(256))
  public fullName: string;

  ////////////////////////////////////////////////////////////////
  // Внешние ключи
  ////////////////////////////////////////////////////////////////

  @ForeignKey(() => Attachment)
  @Column
  public avatarId?: number;

  ////////////////////////////////////////////////////////////////
  // Виртуальные поля
  ////////////////////////////////////////////////////////////////

  @Column(DataType.VIRTUAL)
  public get age(): number {
    if (this.bornAt == null) {
      return undefined;
    }

    const bornAt = DateTime.fromJSDate(this.bornAt);
    const now = DateTime.local();
    const diffInYears = now.diff(bornAt, 'years');

    return Math.floor(diffInYears.as('years'));
  }

  @Column(DataType.VIRTUAL)
  public get avatarUrl(): string {
    return this.avatar?.url ?? 'api/attachments/0_placeholder.png';
  }

  ////////////////////////////////////////////////////////////////
  // Связи
  ////////////////////////////////////////////////////////////////

  @BelongsTo(() => Attachment, 'avatarId')
  public avatar?: Attachment;

  @HasOne(() => LocalUser, 'userId')
  public localUser?: LocalUser;
}
