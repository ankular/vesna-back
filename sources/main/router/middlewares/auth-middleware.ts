import { Request, Response } from 'express';
import memoryCache from 'memory-cache';

import { models } from 'main/database';
import { LocalUser } from 'main/database/models';

export const authMiddleware = {
  initialize() {
    return async (request: Request, response: Response, next) => {
      Object.assign(request.session, {
        currentUser: undefined,
        isAuthenticated: false,
      });

      if (request.cookies.token) {
        const session = await models.Session.findOne({
          include: [
            {
              as: 'localUser',
              include: [
                {
                  as: 'roles',
                  model: models.Role,
                },
              ],
              model: models.LocalUser,
            },
          ],
          where: {
            cookie: request.cookies.token,
          },
        });

        if (session?.localUser != null) {
          Object.assign(request.session, {
            currentUser: session.localUser,
            isAuthenticated: true,
          });

          memoryCache.put('currentUser', session.localUser);
        }
      }

      return next();
    };
  },
  isAuthenticated() {
    return (request: Request, response: Response, next) => {
      if (request.session.isAuthenticated) {
        return next();
      }

      return response
        .status(401)
        .json('Пользователь не авторизован');
    };
  },
  hasRoleCode(roleCode?: string) {
    return (request: Request, response: Response, next) => {
      if (roleCode == null) {
        return next();
      }

      if (!request.session.isAuthenticated) {
        return response
          .status(401)
          .json('Пользователь не авторизован');
      }

      const { roles = [] } = request.session.currentUser as LocalUser;
      const userRoleCodes = roles.map((role) => role.code);

      if (!userRoleCodes.includes(roleCode)) {
        return response
          .status(403)
          .json('Недостаточно прав');
      }

      return next();
    };
  },
};
