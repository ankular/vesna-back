import { Request, Response } from 'express';

export const responseMiddleware = {
  /**
   * Обработчик ошибок, должен вызыватся самым последним
   *
   * @example
   * const express = require('express');
   * const app = express();
   *
   * app.use(responseMiddleware.error());
   */
  error() {
    return (error, request: Request, response: Response, next) => {
      if (!response.headersSent && error != null) {
        if (typeof error !== 'object') {
          error = {
            message: error,
          };
        }

        if (error.message == null || error.message === '') {
          error.message = 'Что-то пошло не так';
        }

        if (error.status == null) {
          error.status = 500;
        }

        request.app.emit('error', error);

        return response
          .status(error.status)
          .json(error.message);
      }

      return next();
    };
  },
  /**
   * Преобразователь ответа в json, должен вызыватся самым первым
   *
   * @example
   * const express = require('express');
   * const app = express();
   *
   * app.use(responseMiddleware.json());
   */
  json() {
    return (request: Request, response: Response, next) => {
      const { json: responseJson } = response;

      response.json = (body) => {
        if (body == null) {
          body = 'Данные отсутствуют';
        }

        if (typeof body !== 'object') {
          body = {
            message: body,
          };
        }

        return responseJson.call(response, body);
      };

      return next();
    };
  },
  /**
   * Обработчик 404, должен вызыватся сразу перед error или самым последним
   *
   * @example
   * const express = require('express');
   * const app = express();
   *
   * app.use(responseMiddleware.notFound());
   */
  notFound() {
    return (request: Request, response: Response) => {
      return response
        .status(404)
        .json(`Маршрут ${request.url} не найден`);
    };
  },
};
