import { Router } from 'express';
import { existsSync, readFileSync, unlinkSync, writeFileSync } from 'fs';
import { join, parse } from 'path';
import sharp from 'sharp';
import { FindOptions } from 'sequelize';

import { models } from 'main/database';

const router = Router();
const uploadDir = join(process.env.PWD, 'attachments');

router.get('/:id(\\d+)_:name', async (request, response) => {
  const getInstance = async () => {
    if (+request.params.id === 0) {
      const sharpOptions: sharp.SharpOptions = {
        create: {
          background: '#e9ecef',
          channels: 3,
          height: 480,
          width: 640,
        },
      };

      const file = await sharp(sharpOptions).toFormat('png').toBuffer();

      return { file, id: 0, isImage: true, mime: 'images/png', name: 'placeholder.png' };
    } else {
      const findOptions: FindOptions = { where: { name: request.params.name } };
      const attachment = await models.Attachment.findByPk(request.params.id, findOptions);
      const attachmentPath = join(attachment.directory, `${attachment.id}${parse(attachment.name).ext}`);
      const file = readFileSync(join(uploadDir, attachmentPath));

      return Object.assign(attachment, { file });
    }
  };

  const fileData = await getInstance();

  if (fileData.isImage) {
    if (request.query.format != null) {
      fileData.file = await sharp(fileData.file).toFormat(sharp.format[request.query.format]).toBuffer();
      fileData.mime = `image/${request.query.format}`;
      fileData.name = `${fileData.name}.${request.query.format}`;
    }

    if (request.query.resize != null) {
      const [width, height] = request.query.resize.split('x').map(Number);

      fileData.file = await sharp(fileData.file)
        .resize(width, height || width, { fit: 'cover', withoutEnlargement: true })
        .toBuffer();
    }
  }

  response.setHeader('Content-Type', `${fileData.mime}`);
  response.setHeader('Content-Disposition', `inline; filename=${fileData.id}_${fileData.name}`);
  response.send(fileData.file);
});

router.post('/', async (req, res) => {
  const { file, ...body } = req.body;
  const attachment = await models.Attachment.create(body);
  const attachmentPath = join(attachment.directory, `${attachment.id}${parse(attachment.name).ext}`);
  const filePath = join(uploadDir, attachmentPath);

  await writeFileSync(filePath, file, { encoding: 'base64' });

  return res.status(201).json(attachment);
});

router.put('/:id(\\d+)', async (req, res) => {
  const { file, ...body } = req.body;
  const attachment = await models.Attachment.findByPk(req.params.id);

  if (file != null) {
    const attachmentPath = join(attachment.directory, `${attachment.id}${parse(attachment.name).ext}`);
    const filePath = join(uploadDir, attachmentPath);

    if (existsSync(filePath)) {
      await unlinkSync(filePath);
    }
  }

  await attachment.update(body);

  if (file != null) {
    const attachmentPath = join(attachment.directory, `${attachment.id}${parse(attachment.name).ext}`);
    const filePath = join(uploadDir, attachmentPath);

    await writeFileSync(filePath, file, { encoding: 'base64' });
  }

  return res.json(attachment);
});

export { router };
