import { Router } from 'express';

import { authMiddleware } from 'main/router/middlewares';
import { models } from 'main/database';

const router = Router();

router.get('/check', authMiddleware.isAuthenticated(), async (request, response) => {
  return response
    .status(200)
    .json(request.session.currentUser);
});

router.put('/sign-in', async (request, response) => {
  const { username, password } = request.body;

  if (username == null || password == null) {
    return response
      .status(409)
      .json('Не переданы имя пользователя и/или пароль');
  }

  const localUser = await models.LocalUser.findByPk(username, {
    attributes: ['id', 'username', 'password'],
  });

  if (localUser == null) {
    return response
      .status(403)
      .json('Пользователя не существует');
  }

  if (!localUser.checkPassword(password)) {
    return response
      .status(403)
      .json('Неверный пароль');
  }

  const cookie = request.session.id;

  await models.Session.create({ cookie, localUserId: localUser.id });

  return response
    .status(202)
    .cookie('token', cookie)
    .json(request.session.currentUser);
});

router.post('/sign-up', async (request, response) => {
  const { username, password, fullName = username } = request.body;

  if (username == null || password == null) {
    return response
      .status(409)
      .json('Не переданы имя пользователя и/или пароль');
  }

  const hasLocalUser = !!(await models.LocalUser.findByPk(username));

  if (hasLocalUser) {
    return response
      .status(409)
      .json('Пользователь уже существует');
  }

  const user = await models.User.create({ fullName });
  const localUser = await models.LocalUser.create({ password, username, userId: user.id });
  const cookie = request.session.id;

  await models.Session.create({ cookie, localUserId: localUser.id });

  return response
    .status(201)
    .cookie('token', cookie)
    .send(request.session.currentUser);
});

router.delete('/sign-out', authMiddleware.isAuthenticated(), async (request, response) => {
  await models.Session.destroy({ where: { cookie: request.cookies.token }});

  return response
    .status(202)
    .clearCookie('token')
    .json('До скорой встречи');
});

export { router };
