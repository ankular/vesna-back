import { Router } from 'express';
import { Resource } from 'modules/crunch';
import { Employee } from 'main/database/models';
import { models } from 'main/database';

const router = Router();

router.post('/', async (req, res) => {
  const { userId, positions } = req.body;
  const resource = await new Resource(Employee).create({ userId, positions });

  const ess = req.body.services = req.body.services.map((bodyService) => {
    return {
      employeeId: resource.id,
      serviceId: bodyService.id,
      leadTime: `${bodyService?.leadTime?.hour ?? '00'}:${bodyService?.leadTime?.minute ?? '00'}:00`,
    };
  });

  await Promise.all(ess.map((es) => models.EmployeeService.create(es)));

  res.json(resource);
});

router.put('/:id', async (req, res) => {
  const { services, user, ...body } = req.body;

  const newES = services
    .filter(({ employeeServices }) => employeeServices.length === 0)
    .map((bodyService) => {
      return {
        employeeId: req.params.id,
        serviceId: bodyService.id,
        leadTime: `${bodyService?.leadTime?.hour ?? '00'}:${bodyService?.leadTime?.minute ?? '00'}:00`,
      };
    });

  const oldES = services
    .filter(({ employeeServices }) => employeeServices.length !== 0)
    .map((bodyService) => {
      bodyService['employeeServices'][0].leadTime = `${bodyService.leadTime.hour}:${bodyService.leadTime.minute}:00`;
      return bodyService['employeeServices'][0];
    });

  const resource = await new Resource(Employee).update(body);
  await Promise.all(oldES.map((es) => models.EmployeeService.update(es, { where: { id: es.id } })));
  await Promise.all(newES.map((es) => models.EmployeeService.create(es)));

  res.json(resource);
});

export { router };
