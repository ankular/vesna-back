import { Router } from 'express';
import { models } from 'main/database';
import { Resource } from 'modules/crunch';

const router = Router();
const { Order } = models;

interface OrderItem {
  clientId: number;
  client;
  discount: number;
  employeeServiceIds;
  executionAt: Date;
}

router.post('/', async (req, res) => {
  const { orderEmployeeServices, ...body } = req.body;
  const order = await Order.create(body);

  // создание новых
  const newOes = orderEmployeeServices.filter(({ id }) => id == null).map((item) => ({ ...item, orderId: order.id }));
  await models.OrderEmployeeService.bulkCreate(newOes);

  // удаление ненужных
  const deletedOesIds = orderEmployeeServices.filter(({ id, deleted }) => id != null && deleted).map(({ id }) => id);
  await models.OrderEmployeeService.destroy({ where: { id: deletedOesIds } });

  res.json('Сохранено');
});

router.put('/:id', async (req, res) => {
  const { orderEmployeeServices, ...body } = req.body;
  await Order.update(body, { where: { id: req.params.id } });

  // создание новых
  const newOes = orderEmployeeServices.filter(({ id }) => id == null).map((item) => ({ ...item, orderId: req.params.id }));
  await models.OrderEmployeeService.bulkCreate(newOes);

  // удаление ненужных
  const deletedOesIds = orderEmployeeServices.filter(({ id, deleted }) => id != null && deleted).map(({ id }) => id);
  await models.OrderEmployeeService.destroy({ where: { id: deletedOesIds } });

  res.json('Сохранено');
});

export { router };
