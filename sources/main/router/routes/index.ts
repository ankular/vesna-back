import { sequelize } from 'main/database';
import { routesGenerator } from 'modules/crunch';

export const routes = routesGenerator({
  dirname: __dirname,
  files: [
    '/api/attachments',
    '/api/clients',
    '/api/employees',
    '/api/employee-services',
    '/api/examples',
    '/api/holidays',
    '/api/orders',
    '/api/positions',
    '/api/roles',
    '/api/service-types',
    '/api/services',
    '/api/time-sheets',
    '/api/users',
  ],
  sequelize,
});
