import SocketIO from 'socket.io';

const socketServer = SocketIO();

socketServer.on('connection', (socket) => {
  socket.on('subscribe', (room) => {
    socket.join(room);
  });

  socket.on('unsubscribe', (room) => {
    socket.leave(room);
  });

  socket.on('update', (request) => {
    socket.broadcast
      .to(request.room)
      .emit(request.event, request);
  });
});

export { socketServer };
