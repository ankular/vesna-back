import { FindOptions, IncludeOptions } from 'sequelize';
import deepmerge from 'deepmerge';

import { getSplitPath } from 'modules/crunch/options-generator/helpers/path-helper';

export type Options = FindOptions | IncludeOptions;

/**
 * Ищет переданный путь в опциях
 * Полученные опции являются мутационными по отношению к переданным
 *
 * @param options исходные опции в которых осуществляется поиск
 * @param path строковый путь для поиска
 * @returns {Options}
 */
export function getPathOptions(options: Options, path: string): Options {
  const splitPath = getSplitPath(path);
  const sourceAs = splitPath.shift(); // первый элемент последовательности
  const include = options.include as IncludeOptions[] ?? [];
  const subOptions = include.find(({ as }) => as === sourceAs);

  if (subOptions == null) { // на выбранном уровне не найден путь
    // выбранный уровень и есть путь
    return sourceAs == null || sourceAs.length === 0 ? options : undefined;
  }

  // ищет на следующем уровне
  return getPathOptions(subOptions, splitPath.join('.'));
}

/**
 * Мерджит опции для создания запросов
 * Первый параметр является мутационным
 *
 * @param {Options} options исходные опции, которые нужно расширить
 * @param {Options} extraOptions дополнительные опции
 * @returns {Options} опции полученные путем слияния параметров
 */
export function mergeOptions(options: Options, extraOptions: Options): Options {
  const customMerge = (key: string) => {
    if (key === 'attributes' || key === 'group') {
      return (targets: Options[], sources: Options[]): Options[] => {
        return Array.from(new Set(targets.concat(sources)));
      };
    }

    if (key === 'include') {
      return (targets: IncludeOptions[], [source]: IncludeOptions[]): IncludeOptions[] => {
        const sourceIndex = targets.findIndex(({ as }) => as === source.as);

        if (sourceIndex === -1) return targets.concat(source); // новые данные

        // склеивает исходную опцию с новой
        targets[sourceIndex] = deepmerge(targets[sourceIndex], source, { customMerge });

        return targets;
      };
    }
  };

  return Object.assign(options, deepmerge(options, extraOptions, { customMerge }));
}
