/**
 * Разбивает путь на строку последовательностей
 *
 * @param {string} path
 * @returns {string[]}
 *
 * @example
 * getSplitPath() -> []
 * getSplitPath('user.localUser') -> ['user', 'localUser']
 */
export function getSplitPath(path?: string): string[] {
  return path?.split('.') ?? [];
}

/**
 * Преобразует путь и/или атрибут в скоуп-путь
 *
 * @param {string} path
 * @param {string} attribute
 * @returns {string}
 *
 * @example
 * getScopePath('user') -> 'user()'
 * getScopePath(undefined, 'fullName') -> 'fullName'
 * getScopePath('user.localUser', 'username') -> 'user(localUser(username))'
 */
export function getScopePath(path: string = '', attribute: string = ''): string {
  return path.length === 0
    ? attribute
    : getSplitPath(path).reduceRight((a, b) => `${b}(${a})`, attribute);
}
