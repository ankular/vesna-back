import { ModelCtor } from 'sequelize-typescript';
import { FindOptions } from 'sequelize';

import { mergeOptions } from 'modules/crunch/options-generator/helpers';
import { orderParser, pagerParser, scopeParser, whereParser } from 'modules/crunch/options-generator/parsers';

export function optionsGenerator(model: ModelCtor, query: string | Record<string, any>) {
  const conditions: FindOptions = {};
  const selections: FindOptions = {};

  if (typeof query === 'string') {
    query = query
      .split('&')
      .map((value) => value.split('='))
      .reduce((acc, [key, value]) => Object.assign(acc, { [key]: value }), {}) as object;
  }

  Object
    .entries(query)
    .map(([key, value]) => `${key}=${value}`)
    .forEach((queryValue) => {
      if (queryValue.startsWith('order')) {
        const orderOptions = orderParser(model, queryValue);

        mergeOptions(conditions, orderOptions);
        mergeOptions(selections, orderOptions);

        return;
      }

      if (queryValue.startsWith('pager')) {
        const pagerOptions = pagerParser(model, queryValue);

        mergeOptions(conditions, pagerOptions);

        return;
      }

      if (queryValue.startsWith('scope')) {
        const scopeOptions = scopeParser(model, queryValue);

        mergeOptions(selections, scopeOptions);

        return;
      }

      if (queryValue.startsWith('where')) {
        const whereOptions = whereParser(model, queryValue);

        mergeOptions(conditions, whereOptions);

        return;
      }
    });

  const defaultConditionOptions: FindOptions = {
    group: [model.primaryKeyAttribute],
    limit: 10,
    offset: 0,
    paranoid: true,
    subQuery: false,
  };

  const defaultSelectionOptions: FindOptions = {
    paranoid: true,
    subQuery: false,
  };

  return {
    conditions: Object.assign({}, defaultConditionOptions, conditions),
    selections: Object.assign({}, defaultSelectionOptions, selections),
  };
}
