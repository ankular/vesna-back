import { ModelCtor } from 'sequelize-typescript';
import { FindOptions, OrderItem } from 'sequelize';

import { getScopePath, getSplitPath, mergeOptions } from 'modules/crunch/options-generator/helpers';
import { scopeParser } from 'modules/crunch/options-generator/parsers/scope-parser';

/**
 * Парсит параметры сортировки для выборки
 *
 * @param {ModelCtor} model
 * @param {string} query
 * @returns {FindOptions}
 */
export function orderParser(model: ModelCtor, query: string): FindOptions {
  const matchedQuery = query.match(/^(?:order\.)?(asc|desc)(?:-([a-zа-яё.]+))?=([a-zа-яё]+)$/i);

  if (matchedQuery == null) {
    throw new Error(`Ошибка парсинга order: ${query}`);
  }

  const [type, path, attribute] = matchedQuery.splice(1);
  const splitPath = getSplitPath(path);
  const scopePath = getScopePath(path);

  const options = scopeParser(model, scopePath);
  const extraOptions: FindOptions = { order: [[...splitPath, attribute, type] as OrderItem] };

  if (path != null) { // если дочерняя выборка, то добавляет сортировку верхнего уровня
    (extraOptions.order as OrderItem[]).push([model.primaryKeyAttribute, 'desc']);
  }

  mergeOptions(options, extraOptions);

  return options;
}
