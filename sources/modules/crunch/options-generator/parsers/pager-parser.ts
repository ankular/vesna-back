import { ModelCtor } from 'sequelize-typescript';
import { FindOptions, IncludeOptions } from 'sequelize';

import { getPathOptions, getScopePath, mergeOptions } from 'modules/crunch/options-generator/helpers';
import { scopeParser } from 'modules/crunch/options-generator/parsers/scope-parser';


/**
 * Парсит параметры количества для выборки
 *
 * @param {ModelCtor} model
 * @param {string} query
 * @returns {FindOptions}
 *
 * @example
 * pagerParser(LocalUser, 'pager.skip-roles=10')
 * pagerParser(LocalUser, 'pager.skip-roles.users=1')
 * pagerParser(User, 'pager.take=10')
 */
export function pagerParser(model: ModelCtor, query: string): FindOptions {
  const matchedQuery = query.match(/^(?:pager\.)?(skip|take)(?:-([\w.]+))?=(\w+)$/);

  if (matchedQuery == null) {
    throw new Error(`Ошибка парсинга pager: ${query}`);
  }

  const [type, path, value] = matchedQuery.splice(1);
  const scopePath = getScopePath(path);

  const options = scopeParser(model, scopePath);

  const pathOptions = getPathOptions(options, path);
  const pathExtraOptions: IncludeOptions = { [type === 'skip' ? 'offset' : 'limit']: +value };

  mergeOptions(pathOptions, pathExtraOptions);

  return options;
}
