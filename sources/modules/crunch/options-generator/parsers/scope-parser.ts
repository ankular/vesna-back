import { ModelCtor } from 'sequelize-typescript';
import { FindOptions, IncludeOptions } from 'sequelize';

/**
 * Нормализует и валидирует грязные опции в опции для выборки
 *
 * @param {ModelCtor} model
 * @param {Record<string, any>} dirtyOptions
 * @returns {FindOptions}
 */
export function getScopeOptions(model: ModelCtor, dirtyOptions: Record<string, any>): FindOptions {
  return Object
    .entries(dirtyOptions)
    .reduce((options: IncludeOptions, [key, value]: [string, string | Record<string, any>]) => {
      if (key.startsWith('attributes_') && typeof value === 'string') {
        const attributes = options.attributes as string[] ?? [];

        if (model.rawAttributes[value] == null) {
          throw new Error(`Не найден атрибут ${value} в модели ${model.name}`);
        }

        return Object.assign(options, { attributes: attributes.concat(value) });
      }

      if (key.startsWith('include_') && typeof value === 'object') {
        const include = options.include ?? [];
        const association = model.associations[value.as]; // находит связь связь

        if (association == null) {
          throw new Error(`Не найдена связь ${value.as} в модели ${model.name}`);
        }

        const subOptions = getScopeOptions(association.target as ModelCtor, value) as IncludeOptions;

        if (association.associationType === 'BelongsToMany') {
          subOptions.through = { attributes: [] }; // убирает третью таблицу из ответа
        }

        return Object.assign(options, { include: include.concat(subOptions) });
      }

      if (key === 'as' && typeof key === 'string') {
        return Object.assign(options, { as: value, model });
      }

      return options;
    }, { paranoid: true, subQuery: false }); // достает все что есть
}

/**
 * Парсит параметры связей и/или атрибутов для выборки
 *
 * @param {ModelCtor} model основная модель, для который будет построены атрибуты и инклуды
 * @param {string} query строка запроса
 * @returns {FindOptions}
 *
 * @example
 * scopeParser(User, 'scope=fullName,localUser(username,roles(name))');
 * scopeParser(User, 'fullName,bornAt');
 */
export function scopeParser(model: ModelCtor, query: string): FindOptions {
  const matchedQuery = query
    .replace(/(\r|\n|\s)/gm, '') // удаляет все пробелы
    .match(/^(?:scope=)?([\w(,)]*)$/); // удаляет из строки ненужное

  if (matchedQuery == null) {
    throw new Error(`Ошибка парсинга scope: ${query}`);
  }

  const [scopePath] = matchedQuery.splice(1); // получает путь

  /**
   * Преобразует путь скоупа в строку, которую можно разпарсить на грязные опции для выборки
   *
   * @type {string}
   */
  const dirtyStringOptions = `{${scopePath}}`
    .replace(/,\)/g, ')') // возможная запятая
    .replace(/([)(])/g, (_, $1) => $1 === '(' ? '{' : '}')
    .replace(/,}$/g, '}') // возможная запятая в самом конце
    .replace(/(\w+){(})?/g, (_, $1, $2 = ',', i) => `"include_${i}":{"as":"${$1}"${$2}`)
    .replace(/([,{])(\w+)/g, (_, $1, $2, i) => `${$1}"attributes_${i}":"${$2}"`);

  try {
    return getScopeOptions(model, JSON.parse(dirtyStringOptions));
  } catch (error) {
    throw new Error(`Ошибка парсинга scope: ${error}`);
  }
}
