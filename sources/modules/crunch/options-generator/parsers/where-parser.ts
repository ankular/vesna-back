import { ModelCtor } from 'sequelize-typescript';
import { FindOptions, IncludeOptions, ProjectionAlias, Op } from 'sequelize';

import { getPathOptions, getScopePath, mergeOptions } from 'modules/crunch/options-generator/helpers';
import { scopeParser } from 'modules/crunch/options-generator/parsers/scope-parser';

/**
 * Парсит условия для выборки
 *
 * @param {ModelCtor} model
 * @param {string} query
 * @returns {FindOptions}
 *
 * where.eq-genres.id=1 только у кого есть жанр с id = 1
 * where.eq-genres.id=1,2 только у кого есть жанры с id = 1 и id = 2
 * where.or-genres.id=1 только у кого есть жанр с id = 1
 * where.or-genres.id=1,2 только у кого есть жанры с id = 1 или id = 2
 * where.neOr-genres.id=1,2 только у кого нет жанров с id 1 или id = 2
 * where.neEq-genres.id=1,2 только у кого нет жанров с id 1 и id = 2
 */
export function whereParser(model: ModelCtor, query: string): FindOptions {
  const matchedQuery = query.match(/^(?:where\.)?(ne)?(eq|or|like|more)(?:-([\w.]+))?(?:[.-]([\w]+))=(.*)/i);

  if (matchedQuery == null) {
    throw new Error(`Ошибка парсинга where: ${query}`);
  }

  const [subType, type, path, attribute, value] = matchedQuery.splice(1);
  const isNegative = subType === 'ne';
  const op = type.toLowerCase();
  const scopePath = getScopePath(path, attribute);

  // TODO сейчас только для первой дочерней связи или главного уровня (where.or-localUser.roles.id=1 не работает)
  const countPath = [path, attribute].filter(Boolean).join('.');
  const countKey = countPath.replace(/(?:\.)(\w)/g, (_, $1) => $1.toUpperCase());

  const options = scopeParser(model, scopePath) as FindOptions;
  const extraOptions: FindOptions = { attributes: [model.primaryKeyAttribute] };

  const pathOptions = getPathOptions(options, path) as IncludeOptions;
  const pathExtraOptions: IncludeOptions = {};

  // TODO http://localhost:6660/api/users?scope=attachments(),localUser(roles())&order.desc=id&where.eq-deletedAt=1997-01-01
  if (['eq', 'or', 'like'].includes(op)) {
    if (op === 'eq' && /(^|,)null(,|$)/i.test(value)) {
      const x = value.replace(/(^|,)null(,|$)/i, (_, $1, $2) => $1 || $2 || '');
      console.log('x', x);
    }

    console.log('TODO проверка на null');

    const ops = {
      eq: `${value.replace(/(?:^|,)([^,]+)/g, '(?=.*(^|,)$1(,|$))')}.*`,
      or: value.replace(/([^,]+)/g, '(^|,)$1(,|$)').replace(/\),/g, ')|'),
      like: `(${value.replace(/,/g, '|')})`,
    };

    (extraOptions.attributes as ProjectionAlias[]).push([`group_concat(${countPath} order by ${countPath})`, countKey]);
    extraOptions.group = [model.primaryKeyAttribute];
    extraOptions.having = { [`$${countKey}$`]: { [isNegative ? Op.notRegexp : Op.regexp]: ops[op] } };
  }

  if (['more'].includes(op)) {
    const ops = { more: isNegative ? 'lt' : 'gt' };

    extraOptions.where = { [attribute]: { [Op[ops[op]]]: value } };
  }

  mergeOptions(pathOptions, pathExtraOptions);
  mergeOptions(options, extraOptions);

  return options;
}
