import { Model, ModelCtor } from 'sequelize-typescript';
import { Transaction } from 'sequelize';

import { optionsGenerator } from 'modules/crunch/options-generator';

export class Resource<T = ModelCtor> {
  public constructor(
    private readonly model: ModelCtor,
  ) {
  }

  /**
   * Удаляет или восстанавливает запись
   *
   * @param {Record<string, any>} values
   * @returns {Promise<Model>}
   */
  public backup(values: Record<string, any>): Promise<Model<T>> {
    return (values as Model<T>).deletedAt != null ? this.restore(values) : this.remove(values);
  }

  /**
   * Создает новую запись и выставляет ей связи (если переданны)
   *
   * @param {Record<string, any>} values
   * @returns {Promise<Model>}
   */
  public create(values: Record<string, any>): Promise<Model<T>> {
    const { primaryKeyAttribute: primaryName } = this.model;

    return this.model.sequelize
      .transaction(async (transaction) => {
        const instance = await this.model.create(values, { transaction });

        await this.updateAssociations(instance, values, transaction);

        return instance;
      })
      .then((instance) => this.get(instance[primaryName]));
  }

  /**
   * Получает информации по одной записи
   *
   * @param {string|number} id
   * @param {string} params
   * @returns {Promise<Model>}
   */
  public get(id: string | number, params: string = ''): Promise<Model<T>> {
    return this.model.sequelize.transaction(async (transaction) => {
      const { primaryKeyAttribute: primaryName } = this.model;
      const { conditions, selections } = optionsGenerator(this.model, params);

      const conditionOptions = Object.assign(conditions, { transaction });
      const instance = await this.model.findByPk(id, conditionOptions);

      if (instance == null || instance[primaryName] == null) {
        throw new Error(`Не найдена запись в ${this.model.name}`);
      }

      const selectionOptions = Object.assign(selections, { transaction });

      return this.model.findByPk(id, selectionOptions);
    });
  }

  /**
   * Получает списочные данные
   *
   * @param {string} params
   * @returns {Promise<{data:Model[],skip:number,take:number,total:number}>}
   */
  public query(params): Promise<{ data: Model<T>[], skip: number, take: number, total: number }> {
    return this.model.sequelize.transaction(async (transaction) => {
      const { primaryKeyAttribute: pkName } = this.model;
      const { conditions, selections } = optionsGenerator(this.model, params);

      const conditionOptions = Object.assign({}, conditions, { transaction });
      const ids = (await this.model.findAll(conditionOptions)).map((instance) => instance[pkName]);

      const selectionOptions = Object.assign({}, selections, { transaction, where: { [pkName]: ids } });
      const instances = await this.model.findAll(selectionOptions);

      const countOptions = Object.assign({}, conditions, { limit: undefined, offset: 0, transaction });
      const instancesCount = await this.model.count(countOptions as any).then((rows) => rows.length);

      return {
        data: instances,
        skip: conditionOptions.offset,
        take: conditionOptions.limit,
        total: instancesCount,
      };
    });
  }

  /**
   * Удаляет запись
   *
   * @param {Record<string, any>} values
   * @returns {Promise<Model>}
   */
  public async remove(values: Record<string, any>): Promise<Model<T>> {
    const { primaryKeyAttribute: primaryName } = this.model;
    const instance = await this.get(values[primaryName]);

    await instance.destroy();

    return instance;
  }

  /**
   * Восстанавливает удаленную модель
   *
   * @param {Record<string, any>} values
   * @returns {Promise<Model>}
   */
  public async restore(values: Record<string, any>) {
    const { primaryKeyAttribute: primaryName } = this.model;
    const instance = await this.get(values[primaryName]); // получает модель по ключу

    await instance.restore();

    return instance;
  }

  /**
   * Обновляет модель вместе со связями
   *
   * @param {Record<string, any>} values
   * @returns {Promise<Model>}
   */
  public update(values: Record<string, any>): Promise<Model<T>> {
    return this.model.sequelize.transaction(async (transaction) => {
      const { primaryKeyAttribute: primaryName } = this.model;
      const instance = await this.get(values[primaryName]); // получает модель по ключу

      await instance.update(values, { transaction }); // обновляет модель
      await this.updateAssociations(instance, values, transaction); // обновляет связи

      // проверяет наличие в данных поля и то что оно null
      if (instance.hasOwnProperty('deletedAt') && instance.deletedAt == null) {
        await instance.restore(); // востанавливает запись из удаленных
      }

      return instance;
    });
  }

  /**
   * Обновляет или создает
   *
   * @param {Record<string, any>} values
   * @returns {Promise<Model>}
   */
  public upsert(values: Record<string, any>): Promise<Model<T>> {
    const { primaryKeyAttribute: primaryName } = this.model;

    return values[primaryName] != null ? this.update(values) : this.create(values);
  }

  /**
   * Обновляет связи для модели
   *
   * @param {Model} instance
   * @param {Record<string, any>} values
   * @param {Transaction} transaction
   * @returns {Promise<Model>}
   */
  private updateAssociations(instance: Model<T>, values: Record<string, any>, transaction: Transaction): Promise<Model<T>> {
    const { associations } = this.model;
    const getSimple = (value, primaryKey: string) => typeof value === 'object' ? value[primaryKey] : value;

    const promises = Object
      .keys(associations)
      .map(async (key) => {
        if (values?.hasOwnProperty(key)) {
          const association = associations[key];
          const { primaryKeyAttribute: primaryName } = association.target;
          const model = this.model.sequelize.model(association.target.name);
          const selfInstance = new model(values[key]);
          const self = { model, updateAssociations: this.updateAssociations };

          await this.updateAssociations.call(self, selfInstance, values[key], transaction);

          if (values[key] == null) { // очистка
            await instance[association['accessors'].set](null, { transaction });
          } else {
            const associationValue = association.isMultiAssociation
              ? values[key].map((value) => getSimple(value, primaryName))
              : getSimple(values[key], primaryName);

            await instance[association['accessors'].set](associationValue, { transaction });
          }
        }

        return instance;
      });

    return Promise
      .all(promises)
      .then(() => instance);
  }
}
