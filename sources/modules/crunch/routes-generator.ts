import { Router } from 'express';
import { lstatSync, readdirSync } from 'fs';
import { join, parse } from 'path';
import { ModelCtor, Sequelize } from 'sequelize-typescript';

import { Resource } from 'modules/crunch/resource';

export interface RouteGeneratorFile {
  model?: ModelCtor;
  path: string;
  router?: Router;
}

export interface RouteGeneratorOptions {
  dirname: string;
  files: (string | RouteGeneratorFile)[];
  sequelize: Sequelize;
}

/**
 * Рекурсивно считывает папку и создает массив из путей
 *
 * @param {string} path
 * @param {string[]} exclude
 * @returns {string[]}
 */
export function readdirSyncRecursive(path: string, exclude: string[] = []): string[] {
  const getFiles = (dirname) => {
    if (lstatSync(dirname).isDirectory()) {
      return readdirSync(dirname)
        .flatMap((filename) => getFiles(join(dirname, filename)));
    }

    if (!exclude.includes(parse(dirname).name)) {
      return [dirname.replace(path, '')];
    }

    return [];
  };

  return getFiles(path);
}

/**
 * Получает RouteGeneratorFile[] от фейковых путей которые были переданы в генератор
 *
 * @param {RouteGeneratorOptions} options
 * @returns {RouteGeneratorFile[]}
 */
export function getFakeRouterFiles(options: RouteGeneratorOptions): RouteGeneratorFile[] {
  return options.files.map((file) => {
    if (typeof file === 'string') return { path: file, router: Router() };
    if (file.router == null) return { ...file, router: Router() };

    return file;
  });
}

/**
 * Получает RouteGeneratorFile[] от реально существующих файлов
 *
 * @param {RouteGeneratorOptions} options
 * @returns {RouteGeneratorFile[]}
 */
export function getRealRouterFiles(options: RouteGeneratorOptions): RouteGeneratorFile[] {
  return readdirSyncRecursive(options.dirname, ['index'])
    .filter((filePath) => /\.[tj]s$/.test(filePath))
    .map((filePath) => {
      const { dir, name } = parse(filePath);
      const { router } = require(join(options.dirname, filePath));

      return { path: join(dir, name), router };
    });
}

/**
 * Мерждит RouteGeneratorFile[]
 *
 * @param {RouteGeneratorFile[]} files
 * @param {RouteGeneratorFile[]} extraFiles
 * @returns {RouteGeneratorFile[]}
 */
export function mergeGeneratorFiles(files: RouteGeneratorFile[], extraFiles: RouteGeneratorFile[]): RouteGeneratorFile[] {
  return files
    .concat(extraFiles)
    .reduce((acc, value) => {
      const index = acc.findIndex(({ path }) => path === value.path); // индекс текущего пути

      if (index === -1) return acc.concat(value);

      if (acc[index].router != null && value.router != null) { // текущий и найденный имеют роутеры
        acc[index].router.stack.push(...value.router.stack);
      }

      return acc;
    }, []);
}

/**
 * Получает пару путь-методы для роутера
 *
 * @param {Router} router
 * @returns {{ path: string, methods: string[] }[]}
 */
export function getRouterStack(router: Router): { path: string, methods: string[] }[] {
  return router.stack.reduce((stack, { route }) => {
    const index = stack.findIndex(({ path }) => path === route.path);
    const methods = Object.keys(route.methods).filter((name) => route.methods[name]); // список методов текущего роута

    if (index === -1) { // такого пути еще нет
      return stack.concat({ path: route.path, methods });
    }

    stack[index].methods.push(...methods); // добавляем методы

    return stack;
  }, []);
}

export function routesGenerator(options: RouteGeneratorOptions): Router {
  const rootRouter = Router();
  const fakeFiles = getFakeRouterFiles(options);
  const realFiles = getRealRouterFiles(options);

  mergeGeneratorFiles(realFiles, fakeFiles)
    .forEach((file) => {
      if (file.model == null) {
        const { name } = parse(file.path);
        const modelName = name.replace(/(?:^|-)(\w)/g, (_, $1) => $1.toUpperCase());

        file.model = Object
          .values(options.sequelize.models)
          .find(({ tableName }) => tableName === modelName) as ModelCtor;
      }

      if (file.model != null) {
        const routerStack = getRouterStack(file.router);
        const resource = new Resource(file.model);


        // просмотр списка
        if (!routerStack.some(({ path, methods }) => path === '/' && methods.includes('get'))) {
          file.router.get('/', async (request, response) => {
            const result = await resource.query(request.query);

            return response
              .status(200)
              .json(result);
          });
        }

        // просмотр детализации
        if (!routerStack.some(({ path, methods }) => path === '/:id(\\d+)' && methods.includes('get'))) {
          file.router.get('/:id(\\d+)', async (request, response) => {
            const result = await resource.get(request.params.id, request.query);

            return response
              .status(200)
              .json(result);
          });
        }

        // создание
        if (!routerStack.some(({ path, methods }) => path === '/' && methods.includes('post'))) {
          file.router.post('/', async (request, response) => {
            const result = await resource.create(request.body);

            return response
              .status(201)
              .json(result);
          });
        }

        // обновление
        if (!routerStack.some(({ path, methods }) => path === '/:id(\\d+)' && methods.includes('put'))) {
          file.router.put('/:id(\\d+)', async (request, response) => {
            const result = await resource.update(request.body);

            return response
              .status(200)
              .json(result);
          });
        }

        // удаление
        if (!routerStack.some(({ path, methods }) => path === '/:id(\\d+)' && methods.includes('delete'))) {
          file.router.delete('/:id(\\d+)', async (request, response) => {
            const { primaryKeyAttribute: primaryName } = file.model;
            const result = await resource.remove({ [primaryName]: request.params['id'] });

            return response
              .status(202)
              .json(result);
          });
        }
      }

      rootRouter.use(file.path, file.router);
    });

  return rootRouter;
}
